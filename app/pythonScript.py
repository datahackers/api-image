from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from pymongo import InsertOne, DeleteOne, ReplaceOne
from bson import json_util 
from Naked.toolshed.shell import execute_js
from request import get

import sys
import json
import pymongo
import requests
import time

idArticle=sys.argv[1]
# print(sys.argv[1])

uri = "mongodb://cwise:AWHwFVsRcHckfuYGikaRfLiMOsUdvopR0LhxN7lllrcOge8PjQoETdrgWpcuCejEY0RREw3Otg5LT0Nr4hd7HQ==@cwise.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"
client = pymongo.MongoClient(uri)
db = client.cwise

#POPULATE JSON WITH URL AND THE ARTICLE ID
def createStructAzure():
    data = json.load(open('structPhrase.json', 'r'))
    return data


#GET THE JSON FROM MONGO
def findDocument(idArticle,db):
    cursor = db.cwise.find({"idArticle":str(idArticle)})
    data=[]
    for document in cursor:
        data = document
    return data

# UPLOAD TO COSMO DB
def uploadDocument(document,db):
    print(document)
    db.cwise.update_one({'idArticle':idArticle},{
    '$set': {
        'media':{
            "0":document["media"]["0"],
            "1":document["media"]["1"],
            "2":document["media"]["2"],
            "3":document["media"]["3"],
            "4":document["media"]["4"],
            "5":document["media"]["5"],
            "6":document["media"]["6"]
        }
    }
    }, upsert=False)

#CALL THE NODEJS SCRIPT
def callNodeJS():
    success = execute_js('get_image.js')
    # time.sleep(2)
    if success:
        success2 = execute_js('get_json_url.js')
        if success2:
            print("node runned well :D ")
        else:
            print("node2 failed")
    else:
        print("node1 failed")

#READ ALL JSON AND ADD THE MONGO ID
def buildLastJSON(tokeep,data):
    with open('all.json') as json_data:
        d = json.load(json_data)
    return d

data = findDocument(idArticle,db)
# print(data)
toKeep = {'_id':data['_id']}
data.pop('_id',None)
json.dump(data, open("all.json", "w"))
callNodeJS()
newData = buildLastJSON(toKeep,data)
uploadDocument(newData,db)


#CALL NEXT API
payload = {'idArticle': idArticle}
r = get('http://52.234.209.185:8004/', params=payload)

